#!/usr/bin/env python3

class colors:
    reset = '\033[0m'
    bold = '\033[01m'
    disable = '\033[02m'
    underline = '\033[04m'
    reverse = '\033[07m'
    strikethrough = '\033[09m'
    invisible = '\033[08m'
    class fg:
        black = '\033[30m'
        red = '\033[31m'
        green = '\033[32m'
        orange = '\033[33m'
        blue = '\033[34m'
        purple = '\033[35m'
        cyan = '\033[36m'
        lightgrey = '\033[37m'
        darkgrey = '\033[90m'
        lightred = '\033[91m'
        lightgreen = '\033[92m'
        yellow = '\033[93m'
        lightblue = '\033[94m'
        pink = '\033[95m'
        lightcyan = '\033[96m'
    class bg:
        black = '\033[40m'
        red = '\033[41m'
        green = '\033[42m'
        orange = '\033[43m'
        blue = '\033[44m'
        purple = '\033[45m'
        cyan = '\033[46m'
        lightgrey = '\033[47m'

class audioplayer:
    def __init__(self, audiofile = 'music.wav'):
        import simpleaudio
        wavefile = simpleaudio.WaveObject.from_wave_file(audiofile)
        play_obj = wavefile.play()

class tanksim:
    entries = [
        'Show tank',
        'Shoot',
        'Drive',
        'Explode',
        'Authors',
        'Changelog',
        'Config',
        'Quit'
    ]
    
    tank = [
        colors.fg.green + '     __|__',
        colors.fg.green + ' __ / **** \\=======#',
        colors.fg.green + ';|HH*T-34*HH:\\',
        colors.fg.darkgrey + '\\=(@=@=@=@=@=/'
    ]

    def clear_screen(self):
        import os
        os.system('cls' if os.name == 'nt' else "printf '\033c'")

    def menu(self):
        import readchar
        
        self.clear_screen()
        
        key = None
        num = 0

        while key != readchar.key.ENTER:
            num_less = num - 1
            num_more = num + 1
            if num == 0:
                num_less = len(self.entries) - 1
                num_more = num + 1
            if num == len(self.entries) - 1:
                num_less = num - 1
                num_more = 0
            
            print(colors.fg.lightred + self.entries[num_less].rjust(30),
                self.entries[num_more].rjust(50) + '\n')
            print(colors.fg.orange + '==>'.rjust(45), colors.bold + self.entries[num].rjust(9),
                colors.fg.orange + '<=='.rjust(6) + colors.reset)

            key = readchar.readkey()
            if key == readchar.key.RIGHT:
                num += 1
            elif key == readchar.key.LEFT:
                num -= 1
            if num > len(self.entries) - 1:
                num = 0
            elif num < 0:
                num = len(self.entries) - 1

            self.clear_screen()
        return num

    def BSOD(self): # FIXME
        import readchar

        print(colors.bg.blue +
            'A problem has been detected and Windows has been shut down to prevent damage to your computer.')
        print('\nThe problem seems to be caused by the following file: ntoskrnl.exe')
        print('\nIRQL_NOT_LESS_OR_EQUAL')
        print('\nIf this is the first time you’ve seen this stop error screen,\nrestart your computer. If this screen appears again, follow\nthese steps:')
        print('\nCheck to make sure any new hardware or software is properly installed.\nIf this is a new installation, ask your hardware or software manufacturer\nfor any Windows updates you might need.')
        print('\nIf problems continue, disable or remove any newly installed hardware\nor software. Disable BIOS memory options such as caching or shadowing.\nIf you need to use safe mode to remove or disable components, restart\nyour computer, press F8 to select Advanced Startup Options, and then\nselect Safe Mode.')
        print('\nTechnical Information:')
        print('\n*** STOP: 0x1000000a (0xf7800d2b, 0x00000002, 0x00000000, 0x805225d7)')
        print('\n*** ntoskrnl.exe – Address 0x805225d7 base at 0x804d7000 DateStamp\n0x4802516a')
        readchar.readkey()

    def tank_show(self):
        import readchar

        for i in self.tank:
            print(i)
        readchar.readkey()
    
    def tank_shoot(self):
        import readchar, shutil, time

        counter = 0
        while counter <= shutil.get_terminal_size()[0] - 22:
            for i in range(len(self.tank)):
                shootline = ' ' * counter + colors.fg.darkgrey + '=>'
                if i == 1:
                    print(self.tank[i] + shootline)
                else:
                    print(self.tank[i])
            counter += 1
            if counter < 4:
                time.sleep(0.2)
            elif 4 <= counter <= 30:
                time.sleep(0.05)
            else:
                time.sleep(0.01)
            self.clear_screen()
        self.tank_show()

    def tank_drive(self): # TODO
        pass

    def tank_explode(self): # TODO
        pass

    def show_authors(self): # TODO
        pass

    def show_changelog(self): # TODO
        pass

    def show_config(self): # TODO
        pass
    
    def __init__(self):
        while True:
            choice = self.menu()
            if choice == 0:
                self.tank_show()
            elif choice == 1:
                self.tank_shoot()
            elif choice == 2:
                self.tank_drive()
            elif choice == 3:
                self.tank_explode()
            elif choice == 4:
                self.show_authors()
            elif choice == 5:
                self.show_changelog()
            elif choice == 6:
                self.show_config()
            elif choice == 7:
                break

tanksim()
